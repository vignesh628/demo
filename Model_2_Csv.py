import os
import requests
import pandas as pd

IP = '10.191.21.20'  # change accordingly
PORT = 8602  # port of ecrash engine server container.

# how to connect E-crash ML server 
def json_to_dataframe(result_json):
    all_pg_list = []    
    for k,v in result_json.items():
        print(k,'_page')
        all_pg_list.extend(v)
    
    result_df = pd.DataFrame(all_pg_list)    
    column_order = ["filename", "label", "score", "xmin", "ymin", "xmax", "ymax", "img_width", "img_height"]
    result_df = result_df[column_order]
    return result_df

def tiff_detection(path_to_tiff,model_name):
    url = "http://{}:{}/predict".format(IP, PORT)  # https may not work
    session=requests.Session()
    session.trust_env=False
    with open(path_to_tiff, "rb") as f:
        tiff_obj = f.read()  # pdf is a byte obeject

    # the payload must have the following fields: "tiff": bytes object, "filename": string object
    # *******************************************************************************************
    payload = {"tiff": tiff_obj,"filename":os.path.basename(path_to_tiff), "model_name": model_name}
    # *******************************************************************************************

    result = session.post(url=url, files=payload).json()


    try:
        print('Total Pages: ' + str(len(result['data'])))
    except KeyError:
        print(result['exception'])
    result_data = result['data']
    result_df=json_to_dataframe(result_data)
    return result_df




#path_to_tiff = '/home/developer/texas_deployment/Sample/305165505.tif'

#result_json = tiff_detection(path_to_tiff,'texas_model')  # model name is second param i.e 'texas_model', 'pipe_model'

#result_df = json_to_dataframe(result_json)

#result_df.to_csv("/home/developer/texas_deployment/Sample/prediction.csv", index=False)
